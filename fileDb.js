const fs = require('fs');
const path = "./messages";
let data = [];
let fileNames = [];
let lastFiveFiles = [];
let lastFiveFilesContent = [];

module.exports = {
    init() {
        try {
             fs.readdir(path, (err, files) => {
                files.forEach(file => {
                    console.log(path + '/' + file);
                    fileNames.push(path + '/' + file);
                });
                 lastFiveFiles = fileNames.sort().splice(-5);
                 for (let i = 0; i < lastFiveFiles.length; i++) {
                     let data = JSON.parse(fs.readFileSync(lastFiveFiles[i]));
                     lastFiveFilesContent.push(data);
                 }
            })
        } catch (e) {
            data = [];
        }
    },
    getMessages() {
        return lastFiveFilesContent;
    },
    addItem(item) {
        item.dateTime = new Date().toISOString();
        data = item;
        fs.writeFileSync('./messages/' + item.dateTime + '.txt', JSON.stringify(data));
    }
};
