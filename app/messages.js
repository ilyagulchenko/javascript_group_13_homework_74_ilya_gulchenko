const express = require('express');
const db = require('../fileDb');

const router = express.Router();

router.get('/', (req, res) => {
    const products = db.getMessages();
    return res.send(products)
});

router.post('/', (req, res) => {
    const message = {
        message: req.body.message,
    };

    db.addItem(message);

    res.send('Your message saved!');
});

module.exports = router;
